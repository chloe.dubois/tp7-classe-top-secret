INSERT INTO Country (country_num, country_label, nationality)
VALUES
(1, 'America', 'American'),
(NULL, 'England', 'English'),
(NULL, 'Finland', 'Finnish'),
(NULL, 'France', 'French'),
(NULL, 'India', 'Indian'),
(NULL, 'Japan', 'Japanese'),
(NULL, 'Norway', 'Norwegian'),
(NULL, 'Iceland', 'Icelandic'),
(NULL, 'Tunisia', 'Tunisian'),
(NULL, 'Iran', 'Iran');

INSERT INTO Mission_type (mission_type_num, mission_type_label)
VALUES
(1, 'Kidnapping'),
(NULL, 'PC Hacking'),
(NULL, 'Rescue'),
(NULL, 'Assassination'),
(NULL, 'Infiltration');

INSERT INTO Mission (mission_code, mission_title, mission_description, start_date, end_date, mission_type_num, country_num)
VALUES
('Snowden', 'Get back the datas', 'We think that the boss of the Big Bro Center uses sensible datas she stole to the entire world. Meet our contact there, he will lead you inside the building to erase the datas.', '2006-12-01', '2020-12-01', 2, 1),
('Turpitudes', 'Kill the king', 'You have to kill the king of the north because he knows nothing.', '2019-09-06', '2020-03-11', 4, 7),
('Jingle Bells', 'Kidnapp Christmas', 'Prove that Santa Claus exists. Stay home to kidnapp him by the time  he will come to give you your yearly orange.', '2020-12-24', '2020-12-25', 1, 3),
('Pet', 'Marry the Beast','You have to go in the Netherlands, find your contact who will help you to marry to the Beast, in order to make him happy for ever.', '1200-01-01', '1740-12-31', 3, 4);

INSERT INTO Hideout_type (hideout_type_num, hideout_type_label)
VALUES
(1, 'Appartment'),
(NULL, 'Bar'),
(NULL, 'Cave'),
(NULL, 'Hut'),
(NULL, 'Igloo'),
(NULL, 'Mension');

INSERT INTO Hideout (hideout_code, hideout_address, hideout_type_num, country_num)
VALUES
('The cut', '186 Fleet Street, London', 3, 2),
('HP', '4, Private Drive Little Whinging, Surrey', 3, 6),
('Meeting', '5, rue des Roses, Montcuq', 2, 4),
('P0wer', '42, Danny the Street', 4, 1),
('BestPlace', '666, Electric Town, Akihabara', 1, 6),
('Freeze', '3, Saint Clara Main Post Office, 96930 Napapiiri', 5, 3);

INSERT INTO Need_hideout (mission_code, hideout_code)
VALUES
('Snowden', 'P0wer'),
('Turpitudes', 'BestPlace'),
('Jingle Bells', 'Freeze'),
('Pet', 'Meeting');

INSERT INTO Status (status_num, status_label)
VALUES
(1, 'To do'),
(NULL, 'Doing'),
(NULL, 'Done'),
(NULL, 'Failed');

INSERT INTO Actualize_status (status_num, mission_code)
VALUES
(2, 'Snowden', ),
(4, 'Turpitudes'),
(1, 'Jingle Bells'),
(3, 'Pet');

INSERT INTO Skill (skill_num, skill_label)
VALUES
(1, 'Impersonation'),
(NULL, 'Karate'),
(NULL, 'Languages and cultures'),
(NULL, 'Mind reading'),
(NULL, 'PC hacking'),
(NULL, 'Talking to animals');

INSERT INTO Require_skill (skill_num, mission_code)
VALUES
(5, 'Snowden'),
(1, 'Snowden'),
(3, 'Turpitudes'),
(2, 'Jingle Bells'),
(6, 'Pet');

INSERT INTO Agent (agent_code, agent_lastname, agent_firstname, agent_birthdate, nationality)
VALUES
('Alpha-B', 'FIJOP', 'Abe', '1771-12-21', 8),
('C3PO', 'SHAROUNA', 'Elsbeth', '1963-09-27', 9),
('Jojo', 'JAW', 'Jonathan', '1983-01-15', 5),
('K177', 'ODA', 'Kirua', '1992-11-14', 6);

INSERT INTO Specialize (skill_num, agent_code)
VALUES
(5 , 'Alpha-B'),
(3 , 'Alpha-B'),
(6 , 'C3PO'),
(2 , 'Jojo'),
(4 , 'Jojo'),
(1 , 'K177');

INSERT INTO Contact (contact_code, contact_lastname, contact_firstname, contact_birthdate, nationality)
VALUES
('Ringu', 'YAMAMURA', 'Sadako', '1991-10-16', 6),
('Omelette du fromage', 'BEAUJOLAIS', 'Blake', '1987-12-10', 4),
('007', 'BRIDGE', 'Jessie', '1975-08-08', 1),
('5had0wSp!k3', 'NEBRA', 'Sebastian', '2000-07-02', 5),
('Mr. Dream', 'KRUEGER', 'Freddy', '1942-02-23', 7);

INSERT INTO Target (target_code, target_lastname, target_firstname, target_birthdate, nationality)
VALUES
('K4D4', 'ABRA', 'Iruna', '1958-08-30', 10),
('The Beast', 'BEER', 'Teddy', '1987-12-12', 5),
('W. White', 'SANTA', 'Claus', '1900-01-01', 3),
('Wolf', 'SUN', 'Johnny', '1978-08-06', 7);

INSERT INTO Assign_agent (mission_code, agent_code)
VALUES
('Jingle Bells', 'Jojo'),
('Pet', 'C3PO'),
('Snowden', 'Alpha-B'),
('Snowden', 'C3PO'),
('Turpitudes', 'K177');

INSERT INTO Solicit_contact (contact_code, mission_code)
VALUES
('Ringu', 'Jingle Bells'),
('Omelette du fromage', 'Pet'),
('007', 'Snowden'),
('Mr. Dream', 'Turpitudes');

INSERT INTO Aim_target (target_code, mission_code)
VALUES
('K4D4', 'Snowden'),
('The Beast', 'Pet'),
('W. White', 'Jingle Bells'),
('Wolf', 'Turpitudes');

INSERT INTO Admin (admin_lastname, admin_firstname, admin_email, admin_password, admin_date)
VALUES
/* mdp d'admin en argon2id, qsdazeaqwzsx */
('GIRSK', 'Ericka', 'Ericka.Girsk@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$JfUekED+wjIMHGlN8dN5bw$sGhi12GM2pporMzez7rNxTIFxYtom+ZjU2z1A8dVx1o', '1976-02-17'),
('PERO', 'Théa', 'thea.pero@orange.fr', '$argon2id$v=19$m=65536,t=4,p=1$JfUekED+wjIMHGlN8dN5bw$sGhi12GM2pporMzez7rNxTIFxYtom+ZjU2z1A8dVx1o', '1959-10-10'),
('DIDIER', 'Michel', 'd.mshell@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$JfUekED+wjIMHGlN8dN5bw$sGhi12GM2pporMzez7rNxTIFxYtom+ZjU2z1A8dVx1o', '1999-03-18'),
('RYÛÛK', 'Nathan', 'Nathanryuuk@gmail.com', '$argon2id$v=19$m=65536,t=4,p=1$JfUekED+wjIMHGlN8dN5bw$sGhi12GM2pporMzez7rNxTIFxYtom+ZjU2z1A8dVx1o', '1987-03-22'),
('DIELBA', 'Niel', 'D.niel@yahoo.fr', '$argon2id$v=19$m=65536,t=4,p=1$JfUekED+wjIMHGlN8dN5bw$sGhi12GM2pporMzez7rNxTIFxYtom+ZjU2z1A8dVx1o', '1963-12-31');