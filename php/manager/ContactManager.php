<?php

require_once './php/manager/DBManager.php';
require_once './php/model/contact.php';

class ContactManager extends DBManager{

    public function getSolicitedContact($contact_code) {
        $stmt_contact = $this->getConnexion()->query('SELECT * FROM Contact JOIN Country ON Contact.nationality = Country.country_num WHERE contact_code ="' . $contact_code . '"');

        $result = $stmt_contact->fetch();
        $contact = new Agent();
        $contact->setCodename($result['contact_code']);
        $contact->setLastname($result['contact_lastname']);
        $contact->setFirstname($result['contact_firstname']);
        $contact->setBirthdate($result['contact_birthdate']);
        $contact->setCountry($result['nationality']);

        return $contact;
        
    }

}

