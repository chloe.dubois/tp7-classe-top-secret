<?php

require './php/manager/DBManager.php';
require './php/model/admin.php';

class AdminManager extends DBManager{
    
    public function getAll() {
        $result = [];

        $stmt_admin = $this->getConnexion()->query('SELECT * FROM Admin');

        while($row_admin = $stmt_admin->fetch()) {
            $admin = new Admin();
            $admin->setNum($row_admin['admin_num']);
            $admin->setLastname($row_admin['admin_lastname']);
            $admin->setFirstname($row_admin['admin_firstname']);
            $admin->setEmail($row_admin['admin_email']);
            $admin->setPassword($row_admin['admin_password']);
            $admin->setDate($row_admin['admin_date']);
            $result[] = $admin;
        }
        return $result;
    }

    public function getAdminByEmail($email) {
       
        $pdo = $this->getConnexion();
        
        try {
            $req = $pdo->prepare("SELECT * FROM Admin WHERE admin_email = :email");
            $req->execute(['email'=>$email]);

            $result = $req->fetch();
            
            return $result;
        } catch (PDOException $e) {
            throw new Exception("Error Processing Request", 1);
        }
    }
}