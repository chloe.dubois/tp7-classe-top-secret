<?php

require_once './php/manager/DBManager.php';
require './php/model/mission.php';

class MissionManager extends DBManager{
    public function getAll() {
        $result = [];

        $stmt_mission = $this->getConnexion()->query('SELECT * FROM Mission JOIN Country ON Mission.country_num = Country.country_num JOIN Mission_type ON Mission.mission_type_num = Mission_type.mission_type_num');

        while($row_mission = $stmt_mission->fetch()) {
            $mission = new Mission();
            $mission->setCodename($row_mission['mission_code']);
            $mission->setTitle($row_mission['mission_title']);
            $mission->setDescription($row_mission['mission_description']);
            $mission->setStartdate($row_mission['start_date']);
            $mission->setEnddate($row_mission['end_date']);
            $mission->setCountry($row_mission['country_label']);
            $mission->setType($row_mission['mission_type_label']);

            $stmt_skill = $this->getConnexion()->query('SELECT skill_label FROM Require_skill JOIN Skill ON Require_skill.skill_num = Skill.skill_num  WHERE mission_code ="' . $row_mission['mission_code'] . '"');
            $skills = $stmt_skill->fetchAll(PDO::FETCH_ASSOC);
            $mission->setSkills($skills);

            $stmt_agent = $this->getConnexion()->query('SELECT agent_code FROM Assign_agent WHERE mission_code ="' . $row_mission['mission_code'] . '"');
            $agents = $stmt_agent->fetchAll(PDO::FETCH_ASSOC);
            $mission->setAgents($agents);

            $stmt_target = $this->getConnexion()->query('SELECT target_code FROM Aim_target WHERE mission_code ="' . $row_mission['mission_code'] . '"');
            $targets = $stmt_target->fetchAll(PDO::FETCH_ASSOC);
            $mission->setTargets($targets);

            $stmt_contact = $this->getConnexion()->query('SELECT contact_code FROM Solicit_contact WHERE mission_code ="' . $row_mission['mission_code'] . '"');
            $contacts = $stmt_contact->fetchAll(PDO::FETCH_ASSOC);
            $mission->setContacts($contacts);

            $stmt_hideout = $this->getConnexion()->query('SELECT hideout_code FROM Need_hideout WHERE mission_code ="' . $row_mission['mission_code'] . '"');
            $hideouts = $stmt_hideout->fetchAll(PDO::FETCH_ASSOC);
            $mission->setHideouts($hideouts);

            $result[] = $mission;
        }
        return $result;
    }

}

