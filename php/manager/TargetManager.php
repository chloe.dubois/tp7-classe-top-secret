<?php

require_once './php/manager/DBManager.php';
require_once './php/model/target.php';

class TargetManager extends DBManager{

    public function getAimedTarget($target_code) {
        $stmt_target = $this->getConnexion()->query('SELECT * FROM Target JOIN Country ON Target.nationality = Country.country_num WHERE target_code ="' . $target_code . '"');

        $result = $stmt_target->fetch();
        $target = new Agent();
        $target->setCodename($result['target_code']);
        $target->setLastname($result['target_lastname']);
        $target->setFirstname($result['target_firstname']);
        $target->setBirthdate($result['target_birthdate']);
        $target->setCountry($result['nationality']);

        return $target;
        
    }

}

