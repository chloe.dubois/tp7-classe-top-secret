<?php

require_once './php/manager/DBManager.php';
require_once './php/model/hideout.php';

class HideoutManager extends DBManager{

    public function getNeededHideout($hideout_code) {
        $stmt_hideout = $this->getConnexion()->query('SELECT * FROM Hideout JOIN Country ON Hideout.country_num = Country.country_num JOIN Hideout_type ON Hideout.hideout_type_num = Hideout_type.hideout_type_num WHERE hideout_code ="' . $hideout_code . '"');

        $result = $stmt_hideout->fetch();
        $hideout = new Hideout();
        $hideout->setCodename($result['hideout_code']);
        $hideout->setAddress($result['hideout_address']);
        $hideout->setType($result['hideout_type_label']);
        $hideout->setCountry($result['country_label']);

        return $hideout;
        
    }

}