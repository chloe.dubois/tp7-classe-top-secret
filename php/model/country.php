<?php

class Country
{
    private int $num;
    private string $label;
    private string $nationality;

    /**
     * Get the value of num
     */ 
    public function getNum(): int
    {
        return $this->num;
    }

    /**
     * Set the value of num
     *
     * @return  self
     */ 
    public function setNum(int $num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get the value of label
     */ 
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * Set the value of label
     *
     * @return  self
     */ 
    public function setLabel(string $label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get the value of nationality
     */ 
    public function getNationality(): string
    {
        return $this->nationality;
    }

    /**
     * Set the value of nationality
     *
     * @return  self
     */ 
    public function setNationality(string $nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    // public function __construct(int $num, string $label, string $nationality)
    // {
    //     $this->num = $num;
    //     $this->label = $label;
    //     $this->nationality = $nationality;
    // }
}