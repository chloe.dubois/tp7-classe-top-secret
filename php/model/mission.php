<?php

class Mission
{
    private string $codename;
    private string $title;
    private string $description;
    private string $startdate;
    private string $enddate;
    private string $type;
    private string $country;
    private array $skills;
    private array $agents;
    private array $targets;
    private array $contacts;
    private array $hideouts;
    private array $status;

    /**
     * Get the value of codename
     */ 
    public function getCodename(): string
    {
        return $this->codename;
    }

    /**
     * Set the value of codename
     *
     * @return  self
     */ 
    public function setCodename(string $codename)
    {
        $this->codename = $codename;

        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of description
     */ 
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */ 
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of startdate
     */ 
    public function getStartdate(): string
    {
        return $this->startdate;
    }

    /**
     * Set the value of startdate
     *
     * @return  self
     */ 
    public function setStartdate(string $startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get the value of enddate
     */ 
    public function getEnddate(): string
    {
        return $this->enddate;
    }

    /**
     * Set the value of enddate
     *
     * @return  self
     */ 
    public function setEnddate(string $enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get the value of type
     */ 
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @return  self
     */ 
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get the value of country
     */ 
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */ 
    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get the value of skills
     */ 
    public function getSkills(): array
    { 
        return $this->skills;
    }

    /**
     * Set the value of skills
     *
     * @return  self
     */ 
    public function setSkills(array $skills)
    {
        foreach ($skills as $skill_required) {
            foreach ($skill_required as $key => $value) {
                if ($key == "skill_label") {
                    $this->skills[] = $value;
                }
            }
        }
        return $this;
    }

    /**
     * Get the value of agents
     */ 
    public function getAgents(): array
    {
        return $this->agents;
    }

    /**
     * Set the value of agents
     *
     * @return  self
     */ 
    public function setAgents(array $agents)
    {
        foreach ($agents as $agent_assigned) {
            foreach ($agent_assigned as $key => $value) {
                if ($key == "agent_code") {
                    $this->agents[] = $value;
                }
            }
        }
        return $this;
    }

    /**
     * Get the value of targets
     */ 
    public function getTargets(): array
    {
        return $this->targets;
    }

    /**
     * Set the value of targets
     *
     * @return  self
     */ 
    public function setTargets(array $targets)
    {
        foreach ($targets as $target_aimed) {
            foreach ($target_aimed as $key => $value) {
                if ($key == "target_code") {
                    $this->targets[] = $value;
                }
            }
        }
        return $this;
    }

    /**
     * Get the value of contacts
     */ 
    public function getContacts(): array
    {
        return $this->contacts;
    }

    /**
     * Set the value of contacts
     *
     * @return  self
     */ 
    public function setContacts(array $contacts)
    {
        foreach ($contacts as $contact_solicited) {
            foreach ($contact_solicited as $key => $value) {
                if ($key == "contact_code") {
                    $this->contacts[] = $value;
                }
            }
        }
        return $this;
    }

    /**
     * Get the value of hideouts
     */ 
    public function getHideouts()
    {
        return $this->hideouts;
    }

    /**
     * Set the value of hideouts
     *
     * @return  self
     */ 
    public function setHideouts($hideouts)
    {
        foreach ($hideouts as $hideout_needed) {
            foreach ($hideout_needed as $key => $value) {
                if ($key == "hideout_code") {
                    $this->hideouts[] = $value;
                }
            }
        }
        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}