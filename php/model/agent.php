<?php

require_once './php/model/people.php';

class Agent extends People 
{
    private array $skills; // ceci est un tableau de skills!!

    /**
     * Get the value of skills
     */ 
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * Set the value of skills
     *
     * @return  self
     */ 
    public function setSkills(array $skills)
    {
        foreach ($skills as $skill_required) {
            foreach ($skill_required as $key => $value) {
                if ($key == "skill_label") {
                    $this->skills[] = $value;
                }
            }
        }
        return $this;
    }

    // public function __construct(string $codename, string $firstname, string $lastname, string $birthdate, int $country, array $missions, array $skills)
    // {
    //     parent::__construct($codename, $firstname, $lastname, $birthdate, $country, $missions);
    //     $this->skills = $skills;
    // }
    
}