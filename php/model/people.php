<?php

// definition de la classe abstraite people. Doit s'utiliser avec un "extends".

abstract class People
{
    private string $codename;
    private string $lastname;
    private string $firstname;
    private string $birthdate;
    private string $country; // ou utiliser l'objet private Country $nationality

        /**
     * Get the value of codename
     */ 
    public function getCodename(): string
    {
        return $this->codename;
    }

    /**
     * Set the value of codename
     *
     * @return  self
     */ 
    public function setCodename(string $codename)
    {
        $this->codename = $codename;

        return $this;
    }

    /**
     * Get the value of lastname
     */ 
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * Set the value of lastname
     *
     * @return  self
     */ 
    public function setLastname(string $lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of firstname
     */ 
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * Set the value of firstname
     *
     * @return  self
     */ 
    public function setFirstname(string $firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get the value of birthdate
     */ 
    public function getBirthdate(): string
    {
        return $this->birthdate;
    }

    /**
     * Set the value of birthdate
     *
     * @return  self
     */ 
    public function setBirthdate(string $birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }


    /**
     * Get the value of country
     */ 
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */ 
    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }

    // public function __construct(string $codename, string $firstname, string $lastname, string $birthdate, int $country, array $missions)
    // {
    //     $this->codename = $codename;
    //     $this->firstname = $firstname;
    //     $this->lastname = $lastname;
    //     $this->birthdate = $birthdate;
    //     $this->country = $country;
    //     $this->missions = $missions;
    // }

}