START TRANSACTION;

-- ============================================================
--   Suppression et création de la base de données 
-- ============================================================
DROP DATABASE IF EXISTS secret_database;
CREATE DATABASE secret_database;
USE  secret_database;


-- ============================================================
--   Création des tables                                
-- ============================================================

CREATE TABLE Country (
    country_num             INT NOT NULL AUTO_INCREMENT,
    country_label           VARCHAR(200),
    nationality             VARCHAR(200),
    CONSTRAINT PK_Country PRIMARY KEY (country_num)
);

CREATE TABLE Mission_type (
    mission_type_num    INT NOT NULL AUTO_INCREMENT,
    mission_type_label  VARCHAR(20) NOT NULL,
    CONSTRAINT PK_Mission_type PRIMARY KEY (mission_type_num)
);

CREATE TABLE Mission (
    mission_code        VARCHAR(30) NOT NULL,
    mission_title       VARCHAR(50) NOT NULL,
    mission_description VARCHAR(500) NOT NULL,
    start_date          DATE NOT NULL,
    end_date            DATE NOT NULL,
    mission_type_num    INT NOT NULL,
    country_num         INT NOT NULL,
    CONSTRAINT PK_Mission PRIMARY KEY (mission_code),
    CONSTRAINT FK_Mission_type FOREIGN KEY (mission_type_num) REFERENCES Mission_type (mission_type_num),
    CONSTRAINT FK_Mission_country FOREIGN KEY (country_num) REFERENCES Country (country_num)
);

CREATE TABLE Hideout_type (
    hideout_type_num    INT NOT NULL AUTO_INCREMENT,
    hideout_type_label  VARCHAR(20) NOT NULL,
    CONSTRAINT PK_Hideout_type PRIMARY KEY (hideout_type_num)
);

CREATE TABLE Hideout (
    hideout_code        VARCHAR(30) NOT NULL,
    hideout_address     VARCHAR(100) NOT NULL,
    hideout_type_num    INT NOT NULL,
    country_num         INT NOT NULL,
    CONSTRAINT PK_Hideout PRIMARY KEY (hideout_code),
    CONSTRAINT FK_Hideout_Hideout_type FOREIGN KEY (hideout_type_num) REFERENCES Hideout_type (hideout_type_num),
    CONSTRAINT FK_Hideout_Country FOREIGN KEY (country_num) REFERENCES Country (country_num)
);

CREATE TABLE Need_hideout (
    mission_code        VARCHAR(30) NOT NULL,
    hideout_code        VARCHAR(30) NOT NULL,
    CONSTRAINT PK_Need_hideout PRIMARY KEY (mission_code, hideout_code),
    CONSTRAINT FK_Need_hideout_Hideout FOREIGN KEY (hideout_code) REFERENCES Hideout (hideout_code),
    CONSTRAINT FK_Need_hideout_Mission FOREIGN KEY (mission_code) REFERENCES Mission (mission_code)
);

CREATE TABLE Status (
    status_num          INT NOT NULL AUTO_INCREMENT,
    status_label        VARCHAR(10) NOT NULL,
    CONSTRAINT PK_Status PRIMARY KEY (status_num)
);

CREATE TABLE Actualize_status (
    status_num          INT NOT NULL AUTO_INCREMENT,
    mission_code        VARCHAR(30) NOT NULL,
    status_date			DATE NOT NULL,
    CONSTRAINT PK_Actualize_status PRIMARY KEY (status_num, mission_code, status_date),
    CONSTRAINT FK_Actualize_status_Status FOREIGN KEY (status_num) REFERENCES Status (status_num),
    CONSTRAINT FK_Actualize_status_Mission FOREIGN KEY (mission_code) REFERENCES Mission (mission_code) 
);

CREATE TABLE Skill (
    skill_num           INT NOT NULL AUTO_INCREMENT,
    skill_label         VARCHAR(30) NOT NULL,
    CONSTRAINT PK_Skill PRIMARY KEY (skill_num)
);

CREATE TABLE Require_skill (
    skill_num           INT NOT NULL AUTO_INCREMENT,
    mission_code        VARCHAR(30) NOT NULL,
    CONSTRAINT PK_Require_skill PRIMARY KEY (skill_num, mission_code),
    CONSTRAINT FK_Require_skill_Skill FOREIGN KEY (skill_num) REFERENCES Skill (skill_num),
    CONSTRAINT FK_Require_skill_Mission FOREIGN KEY (mission_code) REFERENCES Mission (mission_code)   
);

CREATE TABLE Agent (
    agent_code          VARCHAR(30) NOT NULL,
    agent_lastname      VARCHAR(30) NOT NULL,
    agent_firstname     VARCHAR(30) NOT NULL,
    agent_birthdate     DATE NOT NULL,
    nationality         INT NOT NULL,
    CONSTRAINT PK_Agent PRIMARY KEY (agent_code),
    CONSTRAINT FK_Agent_Country FOREIGN KEY (nationality) REFERENCES Country (country_num)
);

CREATE TABLE Specialize (
    skill_num           INT NOT NULL AUTO_INCREMENT,
    agent_code          VARCHAR(30) NOT NULL,
    CONSTRAINT PK_Specialize PRIMARY KEY (skill_num, agent_code),
    CONSTRAINT FK_Specialize_Skill FOREIGN KEY (skill_num) REFERENCES Skill (skill_num),
    CONSTRAINT FK_Specialize_Agent FOREIGN KEY (agent_code) REFERENCES Agent (agent_code)  
);

CREATE TABLE Assign_agent (
    mission_code        VARCHAR(30) NOT NULL,
    agent_code          VARCHAR(30) NOT NULL,
    CONSTRAINT PK_Assign_mission PRIMARY KEY (mission_code, agent_code),
    CONSTRAINT FK_Assign_Mission FOREIGN KEY (mission_code) REFERENCES Mission (mission_code),
    CONSTRAINT FK_Assign_Agent FOREIGN KEY (agent_code) REFERENCES Agent (agent_code)   
);

CREATE TABLE Contact (
    contact_code            VARCHAR(30) NOT NULL,
    contact_lastname        VARCHAR(30) NOT NULL,
    contact_firstname       VARCHAR(30) NOT NULL,
    contact_birthdate       DATE NOT NULL,
    nationality             INT NOT NULL,
    CONSTRAINT PK_Contact PRIMARY KEY (contact_code),
    CONSTRAINT FK_Contact_Country FOREIGN KEY (nationality) REFERENCES Country (country_num)
);

CREATE TABLE Solicit_contact (
    contact_code            VARCHAR(30) NOT NULL,
    mission_code            VARCHAR(30) NOT NULL,
    CONSTRAINT PK_Solicit_contact PRIMARY KEY (contact_code, mission_code),
    CONSTRAINT FK_Solicit_Mission FOREIGN KEY (mission_code) REFERENCES Mission (mission_code),
    CONSTRAINT FK_Solicit_Contact FOREIGN KEY (contact_code) REFERENCES Contact (contact_code)
);

CREATE TABLE Target (
    target_code             VARCHAR(30) NOT NULL,
    target_lastname         VARCHAR(30) NOT NULL,
    target_firstname        VARCHAR(30) NOT NULL,
    target_birthdate       DATE NOT NULL,
    nationality             INT NOT NULL,
    CONSTRAINT PK_Target PRIMARY KEY (target_code),
    CONSTRAINT FK_Target_Country FOREIGN KEY (nationality) REFERENCES Country (country_num)
);

CREATE TABLE Aim_target (
    target_code             VARCHAR(30) NOT NULL,
    mission_code            VARCHAR(30) NOT NULL,
    CONSTRAINT PK_Aim_target PRIMARY KEY (target_code, mission_code),
    CONSTRAINT FK_Aim_Mission FOREIGN KEY (mission_code) REFERENCES Mission (mission_code),
    CONSTRAINT FK_Aim_Target FOREIGN KEY (target_code) REFERENCES Target (target_code)
);

-- CREATE TABLE Admin (
--     admin_num               INT NOT NULL AUTO_INCREMENT,
--     admin_lastname          VARCHAR(30) NOT NULL,
--     admin_firstname         VARCHAR(30) NOT NULL,
--     admin_email             VARCHAR(30) NOT NULL,
--     admin_password          VARCHAR(100) NOT NULL,
--     admin_date              DATE NOT NULL,
--     CONSTRAINT PK_Admin PRIMARY KEY (admin_num)
-- );

CREATE TABLE Admin (
    admin_email             VARCHAR(30),
    admin_lastname          VARCHAR(30) NOT NULL,
    admin_firstname         VARCHAR(30) NOT NULL,
    admin_password          VARCHAR(100) NOT NULL,
    admin_date              DATE NOT NULL,
    CONSTRAINT PK_Admin_email PRIMARY KEY (admin_email)
);

-- ============================================================
--   Insertion des enregistrements
-- ============================================================

INSERT INTO Country (country_num, country_label, nationality)
VALUES
(1, 'America', 'American'),
(NULL, 'England', 'English'),
(NULL, 'Finland', 'Finnish'),
(NULL, 'France', 'French'),
(NULL, 'India', 'Indian'),
(NULL, 'Japan', 'Japanese'),
(NULL, 'Norway', 'Norwegian'),
(NULL, 'Iceland', 'Icelandic'),
(NULL, 'Tunisia', 'Tunisian'),
(NULL, 'Iran', 'Iran');

INSERT INTO Mission_type (mission_type_num, mission_type_label)
VALUES
(1, 'Kidnapping'),
(NULL, 'PC Hacking'),
(NULL, 'Rescue'),
(NULL, 'Assassination'),
(NULL, 'Infiltration');

INSERT INTO Mission (mission_code, mission_title, mission_description, start_date, end_date, mission_type_num, country_num)
VALUES
('Snowden', 'Get back the datas', 'We think that the boss of the Big Bro Center uses sensible datas she stole to the entire world. Meet our contact there, he will lead you inside the building to erase the datas.', '2006-12-01', '2020-12-01', 2, 1),
('Turpitudes', 'Kill the king', 'You have to kill the king of the north because he knows nothing.', '2019-09-06', '2020-03-11', 4, 7),
('Jingle Bells', 'Kidnapp Christmas', 'Prove that Santa Claus exists. Stay home to kidnapp him by the time  he will come to give you your yearly orange.', '2020-12-24', '2020-12-25', 1, 3),
('Pet', 'Marry the Beast','You have to go in the Netherlands, find your contact who will help you to marry to the Beast, in order to make him happy for ever.', '1200-01-01', '1740-12-31', 3, 4);

INSERT INTO Hideout_type (hideout_type_num, hideout_type_label)
VALUES
(1, 'Appartment'),
(NULL, 'Bar'),
(NULL, 'Cave'),
(NULL, 'Hut'),
(NULL, 'Igloo'),
(NULL, 'Mension');

INSERT INTO Hideout (hideout_code, hideout_address, hideout_type_num, country_num)
VALUES
('The cut', '186 Fleet Street, London', 3, 2),
('HP', '4, Private Drive Little Whinging, Surrey', 3, 6),
('Meeting', '5, rue des Roses, Montcuq', 2, 4),
('P0wer', '42, Danny the Street', 4, 1),
('BestPlace', '666, Electric Town, Akihabara', 1, 6),
('Freeze', '3, Saint Clara Main Post Office, 96930 Napapiiri', 5, 3);

INSERT INTO Need_hideout (mission_code, hideout_code)
VALUES
('Snowden', 'P0wer'),
('Turpitudes', 'BestPlace'),
('Jingle Bells', 'Freeze'),
('Pet', 'Meeting');

INSERT INTO Status (status_num, status_label)
VALUES
(1, 'To do'),
(NULL, 'Doing'),
(NULL, 'Done'),
(NULL, 'Failed');

INSERT INTO Actualize_status (status_num, mission_code, status_date)
VALUES
(1, 'Snowden', '2006-12-01'),
(2, 'Snowden', '2010-08-08'),
(2, 'Snowden', '2020-09-20'),
(1, 'Turpitudes', '2019-09-06'),
(2, 'Turpitudes', '2020-01-09'),
(4, 'Turpitudes', '2020-01-07'),
(1, 'Jingle Bells', '2020-09-20'),
(1, 'Pet', '1200-01-01'),
(2, 'Pet', '1450-08-23'),
(3, 'Pet', '1740-12-31');

INSERT INTO Skill (skill_num, skill_label)
VALUES
(1, 'Impersonation'),
(NULL, 'Karate'),
(NULL, 'Languages and cultures'),
(NULL, 'Mind reading'),
(NULL, 'PC hacking'),
(NULL, 'Talking to animals');

INSERT INTO Require_skill (skill_num, mission_code)
VALUES
(5, 'Snowden'),
(1, 'Snowden'),
(3, 'Turpitudes'),
(2, 'Jingle Bells'),
(6, 'Pet');

INSERT INTO Agent (agent_code, agent_lastname, agent_firstname, agent_birthdate, nationality)
VALUES
('Alpha-B', 'FIJOP', 'Abe', '1771-12-21', 8),
('C3PO', 'SHAROUNA', 'Elsbeth', '1963-09-27', 9),
('Jojo', 'JAW', 'Jonathan', '1983-01-15', 5),
('K177', 'ODA', 'Kirua', '1992-11-14', 6);

INSERT INTO Specialize (skill_num, agent_code)
VALUES
(5 , 'Alpha-B'),
(3 , 'Alpha-B'),
(6 , 'C3PO'),
(2 , 'Jojo'),
(4 , 'Jojo'),
(1 , 'K177');

INSERT INTO Contact (contact_code, contact_lastname, contact_firstname, contact_birthdate, nationality)
VALUES
('Ringu', 'YAMAMURA', 'Sadako', '1991-10-16', 6),
('Omelette du fromage', 'BEAUJOLAIS', 'Blake', '1987-12-10', 4),
('007', 'BRIDGE', 'Jessie', '1975-08-08', 1),
('5had0wSp!k3', 'NEBRA', 'Sebastian', '2000-07-02', 5),
('Mr. Dream', 'KRUEGER', 'Freddy', '1942-02-23', 7);

INSERT INTO Target (target_code, target_lastname, target_firstname, target_birthdate, nationality)
VALUES
('K4D4', 'ABRA', 'Iruna', '1958-08-30', 10),
('The Beast', 'BEER', 'Teddy', '1987-12-12', 5),
('W. White', 'SANTA', 'Claus', '1900-01-01', 3),
('Wolf', 'SUN', 'Johnny', '1978-08-06', 7);

INSERT INTO Assign_agent (mission_code, agent_code)
VALUES
('Jingle Bells', 'Jojo'),
('Pet', 'C3PO'),
('Snowden', 'Alpha-B'),
('Snowden', 'C3PO'),
('Turpitudes', 'K177');

INSERT INTO Solicit_contact (contact_code, mission_code)
VALUES
('Ringu', 'Jingle Bells'),
('Omelette du fromage', 'Pet'),
('007', 'Snowden'),
('Mr. Dream', 'Turpitudes');

INSERT INTO Aim_target (target_code, mission_code)
VALUES
('K4D4', 'Snowden'),
('The Beast', 'Pet'),
('W. White', 'Jingle Bells'),
('Wolf', 'Turpitudes');

INSERT INTO Admin (admin_email, admin_lastname, admin_firstname, admin_password, admin_date)
VALUES
('cloclo.dbs@gmail.com', 'Dubois', 'Chloé', '$argon2id$v=19$m=65536,t=4,p=1$0YB3Ir4+1/YUmrIjVS5o0Q$hRKUAcsPdZJwdjmX/lJ84lNQ011IPDUktxbLmajytN0', '1987-10-29'), -- Sasha symphony all the day <3
('dede.eyrl@gmail.com', 'Eyrolles', 'Audrey', '$argon2id$v=19$m=65536,t=4,p=1$oPxHKYTOztr8V/yDpOElGw$vWoIxH9ik8jpfqykoC3yVSbI8FyU01P9IYNbFvXDtm0', '1993-12-15'), -- Symphony of the Night !
('toto.chps@gmail.com', 'Chapuis', 'Thomas', '$argon2id$v=19$m=65536,t=4,p=1$Mqp7Xju+qcnk5GrrdfjDEw$a64AsiNuhkQ2phNS5N9MRm+Haiax+c1/7XYQa6SiyEs', '1994-03-18'), -- Symfony 4 ever <3
('cece.cstl@gmail.com', 'Coustellié', 'Cédric', '$argon2id$v=19$m=65536,t=4,p=1$J7+N4fUX89eQxI4tT6O7XA$YsaPUL619qgoMZUnsYPuTZ9Db7DNtSn7Ayb3IMZmzt4', '1987-03-22'); -- Symphonie pour rêveur !

COMMIT;