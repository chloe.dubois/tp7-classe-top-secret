<?php 
    require './php/manager/AdminManager.php';

    $adminManager = new AgentManager();
    $admins = $adminManager->getAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title></title>
    
    <meta name="title" content="">
    <meta name="description" content=""> 
    
    <!--link rel="icon" type="image/png" sizes="32x32" href=""-->

    <!--feuille de style-->
    <link rel="stylesheet" href="./assets/css/style-prod.css">
    <!--fonts-->
</head>

<body>
  
    
    
    <nav class="navbar navbar-dark bg-dark">
        <a class="navbar-brand d-flex font-weight-bold" href="./index.php">The secret Agency</a>  
        <a id="iconLogin" href="loginAdmin.php">
            <svg width="22px" height="22px" viewBox="0 0 16 16" class="bi bi-lock-fill" fill="white" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.5 9a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-7a2 2 0 0 1-2-2V9z"/>
            <path fill-rule="evenodd" d="M4.5 4a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z"/>
            </svg>
        </a>
    </nav>

    <div class="container my-2">
        <?php
            if($_POST["password"] === "hello") {
                echo "welcome";
            } else {
                echo "wrong password";
            }
        ?>

    </div>                   
    <!-- script -->
    <script src="./node_modules/jquery/dist/jquery.slim.min.js"></script>
    <script src="./node_modules/@popperjs/core/dist/umd/popper.js"></script>
    <script src="./node_modules/bootstrap/dist/js/bootstrap.js"></script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
    </script>

</body>
</html>
